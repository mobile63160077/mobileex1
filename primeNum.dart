// ignore_for_file: file_names

import 'dart:io';
import 'dart:math';

bool isPrime(int num) {
  if (num <= 1) {
    return false;
  }
  if (num == 2) {
    return true;
  }
  if (num % 2 == 0) {
    return false;
  }

  for (int i = 3; i <= sqrt(num); i++) {
    if (num % i == 0) {
      return false;
    }
  }
  return true;
}

void main() {
  print('Enter number: ');
  int num = int.parse(stdin.readLineSync()!);
  if (isPrime(num)) {
    print('$num is prime number.');
  } else {
    print('$num is not prime number.');
  }
}
